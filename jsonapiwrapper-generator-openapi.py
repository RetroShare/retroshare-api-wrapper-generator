#!/usr/bin/python3

import os
import sys
import time
import re

import xml.etree.ElementTree as ET
from string import Template


class MethodParam:
	_type = ''
	_name = ''
	_defval = ''
	_in = False
	_out = False
	_isMultiCallback = False
	_isSingleCallback = False
	_description = ''
	_rfid = ''
	_deprecated = False

	# Get the type on a python readable format for the docstring
	@property
	def type(self):
		return self.translateType(self._type)

	def translateType(self, _type):
		if not isinstance(_type, str):
			if isinstance(_type, list):
				return "pair<" + ",".join(p for p in _type) + ">"
			else:
				print("Error: not supported type of parameter", type(_type))
				sys.exit(1)
		_type = _type.strip()
		# if is a std:: just delete ir
		if _type.startswith("std::"):
			return self.translateType(_type[5:])
		elif _type.startswith("std_"):
			return self.translateType(_type[4:])
		# Transform al types of unsigned int into int
		elif ((_type.startswith("uint") or _type.startswith("int"))
			  and (_type.endswith("_t") or _type.endswith("_t *") or _type.endswith("_t*"))) \
				or (_type == "int") or (_type == "unsigned int") or (_type == "size_t"):
			if ("64" in _type):
				return "integer64"
			return "integer"
		elif _type.startswith("const "):
			return self.translateType(_type[6:])
		elif _type.startswith("RS_DEPRECATED "):
			return self.translateType(_type[len("RS_DEPRECATED "):])
		elif _type == "double":
			return "number"
		elif _type == "float":
			return "number"
		elif _type == "bool":
			return "boolean"
		elif _type == "string" or _type == "str":
			return "string"
		# The same as rstime_t
		elif _type == "chrono::system_clock::time_point":
			return "integer"
		# std::error_condition support
		elif _type == "error_condition":
			return "error_condition"
		# On JSON API "void *" is translated as uint64_t
		elif _type == "void *":
			return self.translateType("uint64_t")
		# Very ugly way to fix the name of the enum. Because in the API reference is FileChunksInfo_ChunkStrategy
		# But when is parsed the class name is just ChunkStrategy
		elif _type == "ChunkStrategy":
			return "FileChunksInfo_ChunkStrategy"
		elif _type == "SliceInfo":
			return "FileChunksInfo_SliceInfo"
		elif _type == "DirData":
			return "RsFileTree_DirData"
		elif _type == "FileData":
			return "RsFileTree_FileData"
		# For some generated classes the :: are replaced by _,
		# for example RsLoginHelper::Location become RsLoginHelper_Location
		# return _type.split("::")[1] if len(_type.split("::")) == 2 and _type.split("::")[0] != "std"  else _type.replace("::", "_")
		return _type.replace("::", "_")

	def recursiveGetArrayParameters(self, type):
		lines = []
		inner = re.search("<(.*)>", type).group(1)
		if type.startswith(("map", "pair")):
			mapTypes = []

			# For inner maps like:
			# map< uint32_t, std_pair< std_string, uint32_t > >
			innerMap = re.search("<(.*)>", inner)
			if innerMap is not None:
				# This loop is to get the inner array wherever are in the array, like:
				# [' uint32_t', ' std_pair<> '] or [' std_pair<> ', ' uint32_t', ]
				mapTypes = inner.replace(innerMap.group(1), '').split(',')
				for x in range(0, len(mapTypes)):
					if '<>' in mapTypes[x]:
						mapTypes[x] = mapTypes[x].replace('<>', '<' + innerMap.group(1) + '>')
			else:
				mapTypes = inner.split(",")

			lines = []
			minIndent = ""
			if type.startswith("map"):
				minIndent = tab
				lines = ["type: array",
						 "items:",
						 minIndent + "type: object",
						 minIndent + "properties:"]
			elif type.startswith("pair"):
				lines = ["type: object",
						 "properties:"]

			minIndent = minIndent + tab + tab
			for y in range(0, len(mapTypes)):
				recursive = self.getOpenApiTypeReference(mapTypes[y])

				# If is a map the construction is {"key" : <key>, "value": <value>}
				if y == 0 and type.startswith("map"):
					lines.append(minIndent + "key:")
				elif y == 1 and type.startswith("map"):
					lines.append(minIndent + "value:")
				# If is a pair the construction is {"first" : <first>, "second": <second>}
				elif y == 0 and type.startswith("pair"):
					lines.append(minIndent + "first:")
				elif y == 1 and type.startswith("pair"):
					lines.append(minIndent + "second:")

				# If the returned object is a list
				if isinstance(recursive, list):
					for x in range(0, len(recursive)):
						if x == 0:
							recursive[x] = minIndent + tab + recursive[x]
						else:
							recursive[x] = minIndent + tab + recursive[x]
					lines.extend(recursive)
				else:
					lines.append(minIndent + tab + recursive)
		else:
			lines = ["type: array", "items:"]
			reference = self.getOpenApiTypeReference(inner)
			if isinstance(reference, list):
				# Add indentation
				for x in range(0, len(reference)):
					reference[x] = tab + reference[x]
				lines.extend(reference)
			else:
				lines.append(tab + reference)
		return lines

	# Return type: xxx for primitive types, else return the $ref: xxx or an array
	def getOpenApiTypeReference(self, type=None):
		if type is None:
			type = self.type
		else:
			type = self.translateType(type)
		primitiveTypes = ("number", "integer", "boolean", "string")
		arraysTypes = ("set", "list", "vector", "map", "pair")
		if type in primitiveTypes:
			return "type: " + type
		elif type.startswith(arraysTypes):
			return self.recursiveGetArrayParameters(type)
		# Some languages like Javascript or Dart doesn't have support for int64
		# So for this types the API return also the int64 number as string
		# https://github.com/RetroShare/RetroShare/pull/1829/files#diff-7f3cb8d6b7da60b8dba356a550e8fb1fR138
		elif "integer64" in type:
			return ["type: object",
					"properties:",
					tab + "xint64:",
					tab + tab + "type: number",
					tab + "xstr64:",
					tab + tab + "type: string",
					]
		elif "error_condition" in type:
			return ["type: object",
					"properties:",
					tab + "errorNumber:",
					tab + tab + "type: integer",
					tab + "errorCategory:",
					tab + tab + "type: string",
					tab + "errorMessage:",
					tab + tab + "type: string",
					]
		elif "base64" in type:
			return ["type: object",
					"properties:",
					tab + "base64:",
					tab + tab + "type: string",
					]

		return "$ref: '#/components/schemas/" + type + "'"

	def getIndentedTypeReference(self, indentation, type=None):
		oref = self.getOpenApiTypeReference(type=type)
		if isinstance(oref, list):
			return ''.join(indentation + line for line in oref)
		else:
			return indentation + oref


class TemplateOwn(Template):
	delimiter = '$%'
	pattern = '''
	\$%(?:
		(?P<escaped>\$\%) |   					# Escape sequence of two delimiters
		(?P<named>[_a-z][_a-z0-9]*)%\$      |   	# delimiter and a Python identifier
		{(?P<braced>[_a-z][_a-z0-9]*)}   |   	# delimiter and a braced identifier
		(?P<invalid>)              				# Other ill-formed delimiter exprs
	)
	'''


# This class generate de documentation for api-wrapper
class Documentation():
	_brief = ''
	_detailed = ''
	_paramsIn = []
	_paramsOut = []
	_return = ''
	_retvalType = ''

	def __init__(self):
		self._paramsIn = []
		self._paramsOut = []

	def generateDetailed(self):
		s = '\n' + tab + tab + tab + tab + tab + tab + tab + tab + tab
		return s.join(
			p._name + ': > ' + s + tab + '(' + p.type + ')' + str(p._description) + '' for p in self._paramsIn)

	def getReturn(self):
		return str(self._return) if self.hasOutParams() else "void return"

	def generateOutputElems(self):
		s = "\n" + tab + tab + tab + tab + tab + tab + tab
		return s.join('[out] ' + p._name + '(' + p.type + ")" + str(p._description) for p in self._paramsOut)

	# Generate the indeted reference to get the return objects serialized
	def generateOutputElemsValues(self):
		s = "\n" + tab + tab + tab
		sprops = "properties:"
		if self.hasRetVal():
			openapiType = MethodParam().getIndentedTypeReference(s + tab + tab, type=self._retvalType, )
			sprops += s + tab + "retval:"
			sprops += openapiType
		if self._paramsOut:
			sprops += ''.join(
				s + tab + p._name + ':' + p.getIndentedTypeReference(s + tab + tab) for p in self._paramsOut)
		return sprops

	# Check if has input parameters
	def hasInParams(self):
		if self._paramsIn:
			return True
		return False

	def hasOutParams(self):
		if self._paramsOut or self.hasRetVal():
			return True
		return False

	def hasRetVal(self):
		return True if self._retvalType != 'void' else False

	# Used for openapi body request parameter properties
	def generateProperties(self):
		s = '\n' + tab + tab + tab
		sprops = s + "properties:"
		return sprops + ''.join(
			s + tab + p._name + ':' + p.getIndentedTypeReference(s + tab + tab) for p in self._paramsIn)


# Some xml objects that stores a type contain inside a type with and rfid
# If it has an rfid mean that is a RsClass (compound or not)
# Used to get the rfid for retVals of an endpoint and RsClasses as [in][out] params for this endpoint
# It stores the rfids found inside the RsClassMap
# etreeTypeObject  = etree object that could contain the refid reference
# param = is the param object that store info about the param. If is None mean that is a retval type
# The type=None is used inside parseMemberClass to solve a bug with the enums
def findRfidOnXml(etreeTypeObject, param=None, type=None):
	rfidsRefs = etreeTypeObject.findall('ref')
	if len(rfidsRefs) > 0:
		for ref in rfidsRefs:
			paramRfid = ref.attrib['refid']
			if paramRfid is not None \
					and paramRfid not in rsClassMap:

				name = ''
				# Only used on the RsClasses marked as [inout] on an endpoint
				if param is not None:
					param._rfid = paramRfid
					print("Found new RS class!", param._type, paramRfid)
					name = param.getOpenApiTypeReference()
				# You can pass the type to store it on the reference.
				if type is not None:
					name = MethodParam().getOpenApiTypeReference(type=type)

				rsClassMap[paramRfid] = name


def getText(e):
	return "".join(e.itertext())


def wrapperDefWrite(string):
	wrappersDefFile.write(string)


# Store api wrapper method inside a wrap class
def storeClass(name, functions):
	print("Store class", name)
	wrapperDefWrite(functions)


def processFile(file):
	try:
		dom1 = ET.parse(file).getroot()
	except FileNotFoundError:
		print('Can\'t open:', file)

	headerFileInfo = dom1[0].findall('location')[0].attrib['file']
	headerRelPath = os.path.dirname(headerFileInfo).split('/')[-1] + '/' + os.path.basename(headerFileInfo)

	# Used to store class and his methods.
	# The methods are inside the dictionary as string after be generated on the template
	# At the end, each class will be stored on the output file using the function storeClasss
	classCache = {}

	for sectDef in dom1.findall('.//memberdef'):
		if sectDef.attrib['kind'] != 'variable' or sectDef.find('.//jsonapi') == None:
			continue

		instanceName = sectDef.find('name').text
		typeName = sectDef.find('type/ref').text

		typeFilePath = sectDef.find('type/ref').attrib['refid']

		try:
			dom2 = ET.parse(doxPrefix + typeFilePath + '.xml').getroot()
		except FileNotFoundError:
			print('Can\'t open:', doxPrefix + typeFilePath + '.xml')

		for member in dom2.findall('.//member'):
			refid = member.attrib['refid']
			methodName = member.find('name').text

			if methodName in NOT_SUPPORTED_METHODS:
				continue

			requiresAuth = True

			defFilePath = refid.split('_')[0] + '.xml'

			# print('Looking for', typeName, methodName, 'into', defFilePath)

			try:
				defDoc = ET.parse(doxPrefix + defFilePath).getroot()
			except FileNotFoundError:
				print('Can\'t open:', doxPrefix + defFilePath)

			memberdef = None
			for tmpMBD in defDoc.findall('.//memberdef'):
				tmpId = tmpMBD.attrib['id']
				tmpKind = tmpMBD.attrib['kind']
				tmpJsonApiTagList = tmpMBD.findall('.//jsonapi')

				if len(tmpJsonApiTagList) != 0 and tmpId == refid and tmpKind == 'function':
					tmpJsonApiTag = tmpJsonApiTagList[0]

					tmpAccessValue = None
					if 'access' in tmpJsonApiTag.attrib:
						tmpAccessValue = tmpJsonApiTag.attrib['access']

					requiresAuth = 'unauthenticated' != tmpAccessValue;

					memberdef = tmpMBD

					break

			if memberdef == None:
				continue

			# File is read and have jsonapi method
			apiPath = '/' + instanceName + '/' + methodName

			# retvalType is the return type of the function, for example void, bool, static bool, virtual bool, etc...
			# Is used to know if the function will return a { retval : TYPE } or not
			retvalType = getText(memberdef.find('type'))
			# Apparently some xml declarations include new lines ('\n') and/or multiple spaces
			# Strip them using python magic
			retvalType = ' '.join(retvalType.split())
			findRfidOnXml(memberdef.find('type'), type=retvalType)

			paramsMap = {}
			orderedParamNames = []

			hasSingleCallback = False
			hasMultiCallback = False
			callbackName = ''
			callbackParams = ''

			# Get params
			for tmpPE in memberdef.findall('param'):
				mp = MethodParam()

				pName = getText(tmpPE.find('declname'))
				tmpDefval = tmpPE.find('defval')
				mp._defval = getText(tmpDefval) if tmpDefval != None else ''
				typeXML = tmpPE.find('type')
				pType = getText(typeXML)

				# Parse of parameter type
				if pType.startswith('const '):
					pType = pType[6:]

				if pType.startswith('std::function'):
					if pType.endswith('&'):
						pType = pType[:-1]
					if pName.startswith('multiCallback'):
						mp._isMultiCallback = True
						hasMultiCallback = True
					elif pName.startswith('callback'):
						mp._isSingleCallback = True
						hasSingleCallback = True
					callbackName = pName
					callbackParams = pType
				else:
					pType = pType.replace('&', '').replace(' ', '')

				# Apparently some xml declarations include new lines ('\n') and/or multiple spaces
				# Strip them using python magic
				pType = ' '.join(pType.split())
				mp._defval = ' '.join(mp._defval.split())

				mp._type = pType
				mp._name = pName

				# Once have the parameter type lets check if is a parameter with rfid, that will point to an RsObject,
				# For example RsGxsForumMsg that contain "subparameters" like RsMsgMetaData mMeta; std::string mMsg;
				# When we want to call a function that use RsGxsForumMsg, now we call the function like:
				# 	rsGxsForumMsg = { mMsg: "message" , mMeta : { mMeta things } }
				# That forces us to look at RS code and see needed params.
				# Instead we would like to just call as:
				# 	rsGxsForumMsg = RsGxsForumMsg("message", MMeta(mMeta things))
				findRfidOnXml(typeXML, mp)

				paramsMap[pName] = mp
				orderedParamNames.append(pName)

			for paramItem in memberdef.findall('.//parameteritem'):
				for tmpPN in paramItem.findall('.//parametername'):
					tmpParam = paramsMap[tmpPN.text]
					tmpD = tmpPN.attrib['direction'] if 'direction' in tmpPN.attrib else ''

					if 'in' in tmpD or 'inout' in tmpD:
						tmpParam._in = True

					if 'out' in tmpD or 'inout' in tmpD:
						tmpParam._out = True

				tmpParam._description = paramItem.find('parameterdescription/para').text

			# Params sanity check
			for pmKey in paramsMap:
				pm = paramsMap[pmKey]
				if not (pm._isMultiCallback or pm._isSingleCallback or pm._in or pm._out):
					print('ERROR', 'Parameter:', pm._name, 'of:', apiPath,
						  'declared in:', headerRelPath,
						  'miss doxygen parameter direction attribute!',
						  defFilePath)
					sys.exit()

			# Debug Print
			print(instanceName, apiPath, retvalType, typeName, methodName)
			for pn in orderedParamNames:
				mp = paramsMap[pn]
				print('\t', mp._type, mp._name, mp._in, mp._out)

			doc = Documentation()

			# Save retvalType
			doc._retvalType = retvalType

			# Get description Brief
			for tmpDESC in memberdef.findall('.//briefdescription/para'):
				doc._brief = tmpDESC.text

			# Get description params like deprecation annotation
			detailedDesc = ''
			for tmpDESC in memberdef.findall('.//xrefsect'):
				txt = tmpDESC.find('xreftitle').text
				if tmpDESC.find('xrefdescription/para') is not None:
					txt += tmpDESC.find('xrefdescription/para').text
				doc._detailed += txt

			# Get input parameters and add also to documentation
			for param in orderedParamNames:
				if paramsMap[param]._in:
					# Add it to detailed description
					doc._paramsIn.append(paramsMap[param])
				if paramsMap[param]._out:
					doc._paramsOut.append(paramsMap[param])

			# Get return annotation and add it to detailed description
			for tmpDESC in memberdef.findall('.//simplesect'):
				if tmpDESC.attrib['kind'] == 'return':
					doc._return = str(tmpDESC.find('para').text)

			substitutionsMap = dict()

			brief = ''
			# Check if the method is async
			if hasMultiCallback or hasSingleCallback:
				brief += "This method is asynchronous. "
			# On "doc._detailed" we can found Deprecation annotations, for example
			brief += doc._detailed if doc._detailed else '' + doc._brief

			substitutionsMap['briefDesc'] = brief
			operationId = "".join(x[0:1].capitalize() + x[1:] for x in apiPath.split("/"))
			substitutionsMap['operationId'] = operationId
			substitutionsMap['detailedDesc'] = doc.generateDetailed()
			substitutionsMap['return'] = doc.getReturn()
			substitutionsMap['outputDescription'] = doc.generateOutputElems()
			substitutionsMap['deprecated'] = 'true' if doc._detailed.lower() == 'deprecated' else 'false'

			substitutionsMap['methodName'] = methodName
			# substitutionsMap['orderedParams'] = ', '.join(param + "=None" for param in orderedParamNames if paramsMap[param]._in)

			substitutionsMap['instanceName'] = instanceName
			substitutionsMap['apiPath'] = apiPath

			paramsIn = False
			requestBody = ''
			if doc.hasInParams():
				reqReference = "Req" + methodName[0:1].capitalize() + methodName[1:]
				requestBody = TemplateOwn(open(SUBREQUESTBODY, 'r').read())
				requestBody = requestBody.safe_substitute(detailedDesc=doc.generateDetailed(),
														  reqReference=reqReference,
														  examples=doc.generateDetailed())
				rsReqRespMap.update({reqReference: doc.generateProperties()})
			substitutionsMap['requestBody'] = requestBody

			subResponses = ''
			if doc.hasOutParams():
				substitution = 'type: object'
				respReference = 'Res' + methodName[0:1].capitalize() + methodName[1:]
				substitution = "$ref: '#/components/schemas/" + respReference + "'"

				subResponses = TemplateOwn(open(SUBRESPONSES, 'r').read())
				subResponses = subResponses.safe_substitute(respReference=substitution)
				rsReqRespMap.update({respReference: doc.generateOutputElemsValues()})
			substitutionsMap['response'] = subResponses

			substitutionsMap[
				'requiresAuth'] = 'security:\n' + tab + tab + tab + tab + '- BasicAuth: []' if requiresAuth else ''
			substitutionsMap[
				'requiresAuthBadResponse'] = "'401':\n" + tab + tab + tab + tab + tab + "$ref: '#/components/responses/UnauthorizedError'" if requiresAuth else ''

			templFilePath = TEMPLATE

			templFile = open(templFilePath, 'r')
			wrapperDef = TemplateOwn(templFile.read())

			tmp = wrapperDef.substitute(substitutionsMap)
			# wrapperDefWrite(tmp)
			# wrappersDefFile.write(tmp)

			if not instanceName in classCache:
				classCache[instanceName] = ''

			classCache[instanceName] += tmp

	for k, v in classCache.items():
		storeClass(k, v)


class RsClass:
	_params = []
	_kind = None

	def __init__(self, name, rfidFile, rfidReference):
		self._params = []
		# self.name = name.split("::")[1] if len(name.split("::")) == 2 and name.split("::")[0] != "std" else name.replace("::", "_")
		self.name = name.replace("::", "_")
		self.rfidFile = rfidFile
		self.rfidReference = rfidReference

	# Not used on this wrapper
	# Generate docstring for the RsClass
	# def generateDocString(self):
	# 	s = '\n' + tab + tab + tab + tab + tab
	# 	txt = s + "description: > "
	# 	txt += s + "".join('\n' + tab + tab + p._name + ': ' + '(' + p.type + ')' for p in self._params)
	# 	return txt
	# def getOrderedParams(self):
	# 	return ", ".join(str(p._name) + " = None" for p in self._params)
	#
	# def getParamAssign(self):
	# 	return "".join('\n' + tab + tab + 'self.' + p._name + ' = ' + p._name for p in self._params)

	# Only work if _kind is enum
	# For openapi specification enum hasn't support to have a string identifier for an int value
	# https://github.com/OAI/OpenAPI-Specification/issues/681
	# We are going to use an openapi extension:
	# https://github.com/Bungie-net/api#extension-properties-on-openapi-specs-or-how-to-generate-much-cooler-clients-for-the-bnet-api-if-you-want-to-take-the-time-to-do-so
	def generateEnumValues(self):
		if self._kind is not "enum":
			return None
		map = {}
		for k, v in self._params[0].items():
			v = v.split("=")[1].strip()
			map[k] = int(v, 0)  # Base 10 int
		return map

	# Generate properties for openapi reference
	def generateProperties(self):
		if self._kind == "typedef":
			s = '\n' + tab + tab
		else:
			s = '\n' + tab + tab + tab + tab
		txt = ''
		for p in self._params:
			txt += s + p._name + ':'
			txt += p.getIndentedTypeReference(s + tab)
		return txt


# Process a rfid if is not on rsClassMap or is not processed before
# If it finish with a _ + 34 len string it call  parseMemberClass else parseCompoundClass
def processNextRfid(rfid):
	# Check if this class is already parsed
	if rfid in rsClassMap and isinstance(rsClassMap[rfid], RsClass):
		return

	# Check if the rfidReference contain the hex hash that indicates a memberdef reference inside the xml file
	if isMemberClassRfid(rfid):
		parseMemberClass(getRfidFile(rfid), rfid)
	else:
		parseCompoundClass(getRfidFile(rfid), rfid)


# Get the "file name" from an rfid parsing the rfid
def getRfidFile(rfid):
	# Check if the rfidReference contain the hex hash that indicates a memberdef reference inside the xml file
	if isMemberClassRfid(rfid):
		return rfid.rsplit('_', 1)[0]
	return rfid


# Return True if it finish with a 34 digit len hash
def isMemberClassRfid(rfid):
	tmp = rfid.rsplit('_', 1)
	if len(tmp) > 1 and tmp[1].__len__() == 34:
		return True
	return False


def getDomFromRfidFile(rfidFile):
	try:
		return ET.parse(doxPrefix + rfidFile + '.xml').getroot()
	except FileNotFoundError:
		print('Can\'t open:', doxPrefix + rfidFile + '.xml on aux RsClass generation')
		sys.exit()


# Get memberdef xml object from an rfid.
# element is the element to search
def getMemberdefFromRfid(rfid, element='memberdef'):
	dom = getDomFromRfidFile(getRfidFile(rfid))
	for md in dom.findall('.//' + element):
		if md.attrib['id'] == rfid:
			return md


# Parse a compound class and return an instance of RsClass with the data
def parseCompoundClass(rfidFile, rfidReference):
	print("Parsing compound class", rfidFile, rfidReference)

	dom = getDomFromRfidFile(rfidFile)
	name = dom.find('.//compoundname').text
	rsClass = RsClass(name, rfidFile, rfidReference)

	# Inner function to parse attributes
	def parseAttributes(sdef, privateMember = False):
		for md in sdef.findall(".//memberdef"):
			# Exception used to parse inner private enums members
			# see: https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/-/issues/4
			if md.attrib['kind'] == 'enum' and privateMember:
				rsClassMap[md.attrib['id']] = name + "_" + md.find('.//name').text
				processNextRfid(md.attrib['id'])
				continue

			# Get all parameters that are variables
			elif md.attrib['kind'] != 'variable':
				continue

			tmpParam = parseMemberdefToMethodParam(md)

			# If rfid is not None just run recursively the parameter creation
			if tmpParam._rfid is not None:
				for rfid in tmpParam._rfid:
					processNextRfid(rfid)

			# Check for hardcoded exceptions
			tmpParam = paramsExceptions(tmpParam, rsClass)

			rsClass._params.append(tmpParam)

	for sdef in dom.findall(".//sectiondef"):
		# Get only public sections
		if sdef.attrib['kind'] in ('public-static-attrib', 'public-attrib'):
			parseAttributes(sdef)
		# Parse private attributes for some exceptional classes
		# see: https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/-/issues/4
		if name in ('ChatId') and sdef.attrib['kind'] in ('private-static-attrib', 'private-attrib', 'private-type'):
			parseAttributes(sdef, True)

	rsClassMap[rfidReference] = rsClass


# Parse a member class, considered all that rfid finish with a _ + 34 len string
def parseMemberClass(rfidFile, rfidReference):
	print("Parsing member class", rfidFile, rfidReference)
	name = None
	md = getMemberdefFromRfid(rfidReference)
	name = md.find('.//name').text
	rsClass = RsClass(name, rfidFile, rfidReference)

	if md.attrib['kind'] == "typedef":
		tmpParam = parseMemberdefToMethodParam(md)
		tmpParam._type = typeDefGetPrimitiveType(md.find('type'), )
		rsClass._params.append(tmpParam)
		rsClass._kind = "typedef"

	elif md.attrib['kind'] == "enum":
		# Very ugly way to fix the name of the enum. Sometimes on RS code the enum is referenced like RsClass::InnerEnum
		# On our code, as we can not use :: we translate the type as "_",
		# for example on the $ref of FileChunksInfo_ChunkStrategy
		# But when the class is parsed the name is just ChunkStrategy because is get from de XML refid
		# So the solution is consult the rsClassMap where we stored previously the result of
		# MethodParam().getOpenApiTypeReference, on this case: $ref: '#/components/schemas/FileChunksInfo_ChunkStrategy'
		# After parse the type it check if the compoundname of the XML where the rfid is FileChunksInfo
		# If is it just change the class name from ChunkStrategy to FileChunksInfo_ChunkStrategy
		type = rsClassMap[rfidReference]
		enumNameProcessed = type.split("/")[-1].split("_") if isinstance(type, str) else ""
		if len(enumNameProcessed) > 1:
			parentName = getMemberdefFromRfid(rfidFile, element="compounddef").find("compoundname").text
			if parentName == enumNameProcessed[0]:
				newName = parentName + "_" + rsClass.name
				print("Correction of enum name from " + rsClass.name + " to " + newName)
				rsClass.name = newName

		rsClass._kind = "enum"
		enumValues = {}
		initializer = 0
		for val in md.findall('.//enumvalue'):
			# Todo: some enums hasn't initializer, see rsevents_8h_1aebdc15781f2ab61f46840db2bff25ee9
			if val.find('initializer') is not None and val.find('name') is not None:
				enumValues[val.find('name').text] = val.find('initializer').text
				initializer += 1  # Used to initialize MAX values that are not initialized (see RsEventType in rsevents.h)
			# For some other initializers are not none. The first ocurrency will be 0 and the followings +1
			elif val.find('initializer') is None and val.find('name') is not None:
				enumValues[val.find('name').text] = "= " + str(initializer)
				initializer += 1

		rsClass._params.append(enumValues)

	elif md.attrib['kind'] == "define":
		# Define are not needed
		return
	else:
		print("Error: not supported class kind: ", md.attrib['kind'])
		sys.exit(1)
	rsClassMap[rfidReference] = rsClass


# Function that recursively dig onto a memberdef type until found it primitive type
def typeDefGetPrimitiveType(typeTag, parentRfid=None):
	rfid = typeTag.find('ref').attrib['refid'] if typeTag.find('ref') != None else None

	# If is a pair get the inner rfid references to store it
	if typeTag.text is not None and typeTag.text.startswith("std::pair"):
		pairTypes = []
		for ref in typeTag.findall('ref'):
			processNextRfid(ref.attrib['refid'])
			pairTypes.append(ref.text)
		return pairTypes

	# If rfid is not None just run recursively the parameter creation
	elif rfid is not None:
		if rfid in PRIMITIVECLASS_MAP:
			return PRIMITIVECLASS_MAP[rfid]
		# Sometimes happen that the "memberdef" rfid is the same as the type "rfid"
		# See for example p3gossipdiscovery_8h_1a6be3a190b47f7623fac9e537acee1b55 where the definition is:
		# using RsPeerId =  RsPeerId
		# https://github.com/RetroShare/RetroShare/issues/1602
		elif parentRfid is not None and parentRfid == rfid:
			print("Error parsing typeDefGetPrimitiveType: memberdef.rfid == type.rfid on")
			print(parentRfid)
			sys.exit(1)
		else:
			md = getMemberdefFromRfid(rfid)
			# There are some 'define' inside the typedef definitions, they are translated as int
			if 'define' in md.attrib['kind']:
				return "int"
			return typeDefGetPrimitiveType(md.find('type'), rfid)
	else:
		return getText(typeTag)


# Parse information in a memberdef object to get viarious data
def parseMemberdefToMethodParam(md):
	tmpParam = MethodParam()
	tmpParam._name = md.find('name').text
	tmpDefval = md.find('defval')
	tmpParam._defval = getText(tmpDefval) if tmpDefval != None else ''
	tmpParam._type = getText(md.find('type'))
	tmpParam._description = getText(md.find('briefdescription'))

	# Get multiple rfids of the param type definition
	tmpParam._rfid = None
	rfidsRefs = md.findall('type/ref')
	if len(rfidsRefs) > 0:
		tmpParam._rfid = []
		for ref in rfidsRefs:
			# If is deprecated don't store the rfid because RS_DEPRECATED points to a 'define' type, not used in this generator
			if 'RS_DEPRECATED' in ref.text:
				tmpParam._deprecated = True
				continue
			rfid = ref.attrib['refid']
			tmpParam._rfid.append(rfid)
			# Store it on rsClassMap
			# Needed to solve enum bugs on parseMemberClass function
			if rfid not in PRIMITIVECLASS_MAP and rfid not in rsClassMap:
				rsClassMap[rfid] = tmpParam.getOpenApiTypeReference()
	return tmpParam

# todo: check compatibility with int64 support
# This function is used to hardcode params exceptions when compound classes are parsed
# For example on RsGxsImage the param mData is an  uint8_t* but in reality the JSON API return a string (because is a base64 encoded image)
def paramsExceptions(tmpParam, rsClass):
	if rsClass.name == "RsGxsImage" and tmpParam._name == "mData":
		tmpParam._type = 'base64'
	# See https://gitlab.com/jpascualsana/retroshare-api-wrapper-generator/-/issues/4
	elif rsClass.name == "ChatId" and tmpParam._type == "Type":
		tmpParam._type = 'ChatId_Type'
	return tmpParam

def initializeClassesFile():
	# Initialize components section
	wrapperDefWrite(open(COMPONENTSTMPL, 'r').read())

	# Initialize schemas section
	wrapperDefWrite('\n' + tab + "schemas:")

	# Write the requests objects
	templFile = open(TEMPLATE_REQUESTS, 'r')
	wrapperDef = TemplateOwn(templFile.read())
	for reqName, reqProps in rsReqRespMap.items():
		substitutionsMap = dict()
		substitutionsMap["reqName"] = reqName
		substitutionsMap["properties"] = reqProps

		wrapperDefWrite(wrapperDef.substitute(substitutionsMap))


# Parse de map rsClassFileMap with the detected compound classes
def parseAuxClasses():
	tmpClassMap = rsClassMap.copy()
	for rfid in tmpClassMap:
		processNextRfid(rfid)

	# Need to do it in different loops due to the inner compound classes that are add after parsing
	for cls in rsClassMap.values():
		if not isinstance(cls, RsClass):
			continue
		substitutionsMap = dict()
		substitutionsMap["className"] = cls.name
		templFilePath = None

		if cls._kind == "enum":
			substitutionsMap["className"] = cls.name
			substitutionsMap["enumValues"] = ''
			substitutionsMap["enumNames"] = ''
			s = '\n' + tab + tab + tab + tab + tab
			for k, v in cls.generateEnumValues().items():
				substitutionsMap["enumValues"] += s + "- " + str(v)
				substitutionsMap["enumNames"] += s + "- " + k
			templFilePath = TEMPLATE_RSCLASS_ENUM

		elif cls._kind == "typedef":
			param = cls._params[0]
			if isinstance(param._type, list):
				wrapperDefWrite(cls.generateProperties())
			elif isinstance(param._type, str):
				wrapperDefWrite(cls.generateProperties())
			else:
				print("Error: not supported type of parameter", type(param._type))
			continue
		else:
			substitutionsMap["className"] = cls.name
			substitutionsMap["properties"] = cls.generateProperties()
			templFilePath = TEMPLATE_RSCLASS

		print("Storing RsClass:", cls.name)
		templFile = open(templFilePath, 'r')
		wrapperDef = TemplateOwn(templFile.read())
		wrapperDefWrite(wrapperDef.substitute(substitutionsMap))


if len(sys.argv) != 2:
	print('Usage:', sys.argv[0], 'XML_PATH')
	sys.exit()

start_time = time.time()

xmlPath = str(sys.argv[1])
doxPrefix = xmlPath + '/xml/'

OUTPUTLIB = 'wrapper_openapi.yml'  # Output of the api wrapper library
OUTPUTCLASSWRAPPER = 'wrapper_class_openapi.py'  # Output of the RsClass wrapper

TEMPLATE = 'tmpl/openapi/openapi-paths.tmpl'  # Tamplate for sync functions
SUBREQUESTBODY = 'tmpl/openapi/openapi-sub-requestbody.tmpl'  # Template that get RequestBody structure for openapi
SUBRESPONSES = 'tmpl/openapi/openapi-sub-responses.tmpl'  # Template that get responses structure for openapi
TEMPLATE_RSCLASS = 'tmpl/openapi/openapi-rsclass.tmpl'  # Template for RsClass wrapper
TEMPLATE_RSCLASS_ENUM = 'tmpl/openapi/openapi-rsclass-enum.tmpl'  # Template for RsClass enum type
HEADERTMPL = 'tmpl/openapi/openapi-header.tmpl'  # Header template for api wrapper library
COMPONENTSTMPL = 'tmpl/openapi/openapi-components.tmpl'  # Initialize components section
TEMPLATE_REQUESTS = 'tmpl/openapi/openapi-requests-responses.tmpl'  # Template for requests

# Used to create RsClasses, this map the RS metaclasses to python primitive types
# Where the key is the rfid of the data type in question
PRIMITIVECLASS_MAP = {
	'structt__RsGenericIdType': 'str',
	'classt__RsFlags32': 'int',
	# This is needed due a bug on doxygen parse:
	# https://github.com/RetroShare/RetroShare/issues/1602
	'p3gossipdiscovery_8h_1a6be3a190b47f7623fac9e537acee1b55': 'str',  # RsPeerId
	'p3gossipdiscovery_8h_1ac237a6e20b1ea729aa9d585dd3514f07': 'str',  # RsPgpId
}

# Methods that should be implemented manually or not supported for openapi impl
NOT_SUPPORTED_METHODS = ("registerEventsHandler")

tab = "    "  # Used to solve indentation spaces when template is filled

try:
	wrappersDefFile = open(OUTPUTLIB, 'w')
except FileNotFoundError:
	print('Can\'t open:', OUTPUTLIB)

wrapperDefWrite(open(HEADERTMPL, 'r').read())

# Used to store the parameters that maybe are classes with mMeta, for example rsGxsForumMsg
rsClassMap = {}
# It stores each request and response to create an independent object referencable with $ref on components section
rsReqRespMap = {}

try:
	for file in os.listdir(doxPrefix):
		if file.endswith("8h.xml"):
			processFile(os.path.join(doxPrefix, file))
except FileNotFoundError:
	print('Can\'t list:', doxPrefix)

# try:
# 	wrappersDefFile = open(OUTPUTCLASSWRAPPER, 'w')
# except FileNotFoundError:
# 	print('Can\'t open:', OUTPUTCLASSWRAPPER)

initializeClassesFile()  # Add header to the RsClass wrapper like imports and others
parseAuxClasses()

print("--- %s seconds ---" % (time.time() - start_time))
