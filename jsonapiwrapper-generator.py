#!/usr/bin/python3

import os
import sys
import time

import xml.etree.ElementTree as ET
from string import Template


class MethodParam:
	_type = ''
	_name = ''
	_defval = ''
	_in = False
	_out = False
	_isMultiCallback = False
	_isSingleCallback = False
	_description = ''
	_rfid = ''

	# Get the type on a python readable format for the docstring
	@property
	def type(self):
		return self.translateType(self._type)

	def translateType(self, _type):
		if not isinstance(_type, str):
			if isinstance(_type, list):
				return "dict of (" + ",".join(p for p in _type) + ")"
			else:
				print("Error: not supported type of parameter", type(_type))
				sys.exit(1)
		# if is a std:: just delete ir
		elif _type.startswith("std::"):
			return self.translateType(_type[5:])
		# Transofrm al types of unsigned int into int
		elif (_type.startswith("uint") or _type.startswith("int")) and _type.endswith("_t"):
			return "int"
		elif _type == "double":
			return "float"
		elif _type == "string":
			return "str"
		# For some generated classes the :: are replaced by _,
		# for example RsLoginHelper::Location become RsLoginHelper_Location
		return _type.replace("::", "_")

class TemplateOwn(Template):
	delimiter = '$%'
	pattern = '''
	\$%(?:
		(?P<escaped>\$\%) |   					# Escape sequence of two delimiters
		(?P<named>[_a-z][_a-z0-9]*)%\$      |   	# delimiter and a Python identifier
		{(?P<braced>[_a-z][_a-z0-9]*)}   |   	# delimiter and a braced identifier
		(?P<invalid>)              				# Other ill-formed delimiter exprs
	)
	'''

# This class generate de documentation for api-wrapper
class Documentation():
	_brief = ''
	_detailed = ''
	_paramsIn = []
	_paramsOut = []
	_return = ''

	def __init__(self):
		self._paramsIn = []
		self._paramsOut = []

	def generateDetailed(self):
		txt = ""
		for p in self._paramsIn:
			# Class type for compound classes (inside the RsClass package)
			txt += '\n' + tab + tab + ':param ' + p._name + ': (' + p.type + " " + ") " + str(p._description)
			# If the compund classes are inside a package return packagename.classname
			txt += '\n' + tab + tab + ':type ' + p._name + ': ' + (':py:class:`' + OUTPUTCLASSWRAPPER.split(".py")[0] + "." + p.type + "`" + p.type)
		return txt

	def getReturn(self):
		return ':returns: ' + str(self._return)

	def generateOutputElems(self):
		return "".join('\n' + tab + tab + tab + '[out] ' + p._name + ':(' + p.type + ")" + str(p._description) for p in self._paramsOut)

def getText(e):
	return "".join(e.itertext())

def wrapperDefWrite(string):
	wrappersDefFile.write(string)

# Store api wrapper method inside a wrap class
def storeClass(name, functions):
	print("Store class", name)
	wrapperDefWrite("class {}(object):".format(name[0].upper()+name[1:]) + "\n")
	wrapperDefWrite(functions)

def processFile(file):
	try:
		dom1 = ET.parse(file).getroot()
	except FileNotFoundError:
		print('Can\'t open:', file)

	headerFileInfo = dom1[0].findall('location')[0].attrib['file']
	headerRelPath = os.path.dirname(headerFileInfo).split('/')[-1] + '/' + os.path.basename(headerFileInfo)

	# Used to store class and his methods.
	# The methods are inside the dictionary as string after be generated on the template
	# At the end, each class till be stored on the output file using the function storeClasss
	classCache = {}

	for sectDef in dom1.findall('.//memberdef'):
		if sectDef.attrib['kind'] != 'variable' or sectDef.find('.//jsonapi') == None:
			continue

		instanceName = sectDef.find('name').text
		typeName = sectDef.find('type/ref').text

		typeFilePath = sectDef.find('type/ref').attrib['refid']

		try:
			dom2 = ET.parse(doxPrefix + typeFilePath + '.xml').getroot()
		except FileNotFoundError:
			print('Can\'t open:', doxPrefix + typeFilePath + '.xml')

		for member in dom2.findall('.//member'):
			refid = member.attrib['refid']
			methodName = member.find('name').text

			requiresAuth = True

			defFilePath = refid.split('_')[0] + '.xml'

			# print('Looking for', typeName, methodName, 'into', defFilePath)

			try:
				defDoc = ET.parse(doxPrefix + defFilePath).getroot()
			except FileNotFoundError:
				print('Can\'t open:', doxPrefix + defFilePath)

			memberdef = None
			for tmpMBD in defDoc.findall('.//memberdef'):
				tmpId = tmpMBD.attrib['id']
				tmpKind = tmpMBD.attrib['kind']
				tmpJsonApiTagList = tmpMBD.findall('.//jsonapi')

				if len(tmpJsonApiTagList) != 0 and tmpId == refid and tmpKind == 'function':
					tmpJsonApiTag = tmpJsonApiTagList[0]

					tmpAccessValue = None
					if 'access' in tmpJsonApiTag.attrib:
						tmpAccessValue = tmpJsonApiTag.attrib['access']

					requiresAuth = 'unauthenticated' != tmpAccessValue;

					memberdef = tmpMBD

					break

			if memberdef == None:
				continue

			# File is read and have jsonapi method
			apiPath = '/' + instanceName + '/' + methodName

			retvalType = getText(memberdef.find('type'))
			# Apparently some xml declarations include new lines ('\n') and/or multiple spaces
			# Strip them using python magic
			retvalType = ' '.join(retvalType.split())

			paramsMap = {}
			orderedParamNames = []

			hasInput = False
			hasOutput = False
			hasSingleCallback = False
			hasMultiCallback = False
			callbackName = ''
			callbackParams = ''

			# Get params
			for tmpPE in memberdef.findall('param'):
				mp = MethodParam()

				pName = getText(tmpPE.find('declname'))
				tmpDefval = tmpPE.find('defval')
				mp._defval = getText(tmpDefval) if tmpDefval != None else ''
				typeXML = tmpPE.find('type')
				pType = getText(typeXML)

				# Parse of parameter type
				if pType.startswith('const '):
					pType = pType[6:]

				if pType.startswith('std::function'):
					if pType.endswith('&'):
						pType = pType[:-1]
					if pName.startswith('multiCallback'):
						mp._isMultiCallback = True
						hasMultiCallback = True
					elif pName.startswith('callback'):
						mp._isSingleCallback = True
						hasSingleCallback = True
					callbackName = pName
					callbackParams = pType
				else:
					pType = pType.replace('&', '').replace(' ', '')

				# Apparently some xml declarations include new lines ('\n') and/or multiple spaces
				# Strip them using python magic
				pType = ' '.join(pType.split())
				mp._defval = ' '.join(mp._defval.split())

				mp._type = pType
				mp._name = pName

				# Once have the parameter type lets check if is a parameter with rfid, that will point to an RsObject,
				# For example RsGxsForumMsg that contain "subparameters" like RsMsgMetaData mMeta; std::string mMsg;
				# When we want to call a function that use RsGxsForumMsg, now we call the function like:
				# 	rsGxsForumMsg = { mMsg: "message" , mMeta : { mMeta things } }
				# That forces us to look at RS code and see needed params.
				# Instead we would like to just call as:
				# 	rsGxsForumMsg = RsGxsForumMsg("message", MMeta(mMeta things))
				paramRfid = typeXML.find('ref').attrib['refid'] if typeXML.find('ref') != None else None
				if paramRfid is not None \
						and paramRfid not in rsClassMap:

					mp._rfid = paramRfid

					print("Found new RS class!", pType, paramRfid)

					rsClassMap[paramRfid] = pName

				paramsMap[pName] = mp
				orderedParamNames.append(pName)

			for paramItem in memberdef.findall('.//parameteritem'):
				for tmpPN in paramItem.findall('.//parametername'):
					tmpParam = paramsMap[tmpPN.text]
					tmpD = tmpPN.attrib['direction'] if 'direction' in tmpPN.attrib else ''

					if 'in' in tmpD:
						tmpParam._in = True
						hasInput = True
					if 'out' in tmpD:
						tmpParam._out = True
						hasOutput = True
				tmpParam._description = paramItem.find('parameterdescription/para').text

			# Params sanity check
			for pmKey in paramsMap:
				pm = paramsMap[pmKey]
				if not (pm._isMultiCallback or pm._isSingleCallback or pm._in or pm._out):
					print('ERROR', 'Parameter:', pm._name, 'of:', apiPath,
							  'declared in:', headerRelPath,
							  'miss doxygen parameter direction attribute!',
							  defFilePath)
					sys.exit()

			# Debug Print
			print(instanceName, apiPath, retvalType, typeName, methodName)
			for pn in orderedParamNames:
				mp = paramsMap[pn]
				print('\t', mp._type, mp._name, mp._in, mp._out)

			doc = Documentation()
			# Get description Brief
			for tmpDESC in memberdef.findall('.//briefdescription/para'):
				doc._brief = tmpDESC.text

			# Get description params like deprecation annotation
			detailedDesc = ''
			for tmpDESC in memberdef.findall('.//xrefsect'):
				txt = tmpDESC.find('xreftitle').text
				if tmpDESC.find('xrefdescription/para') is not None:
					txt += tmpDESC.find('xrefdescription/para').text
				doc._detailed += txt

			# Get input parameters and add also to documentation
			for param in orderedParamNames:
				if paramsMap[param]._in:
					# Add it to detailed description
					doc._paramsIn.append(paramsMap[param])
				elif paramsMap[param]._out:
					doc._paramsOut.append(paramsMap[param])

			# Get return annotation and add it to detailed description
			for tmpDESC in memberdef.findall('.//simplesect'):
				if tmpDESC.attrib['kind'] == 'return':
					doc._return = str(tmpDESC.find('para').text)

			substitutionsMap = dict()
			substitutionsMap['briefDesc'] = doc._brief
			substitutionsMap['detailedDesc'] = doc.generateDetailed()
			substitutionsMap['return'] = doc.getReturn() + doc.generateOutputElems()

			substitutionsMap['methodName'] = methodName
			substitutionsMap['orderedParams'] = ', '.join(param + "=None" for param in orderedParamNames if paramsMap[param]._in)

			substitutionsMap['instanceName'] = instanceName
			substitutionsMap['apiPath'] = apiPath
			substitutionsMap['requiresAuth'] = 'True' if requiresAuth else 'False'

			templFilePath = ''
			if hasMultiCallback  or hasSingleCallback:
				templFilePath += TEMPLATE_ASYNC
			else:
				templFilePath += TEMPLATE

			templFile = open(templFilePath, 'r')
			wrapperDef = TemplateOwn(templFile.read())

			tmp = wrapperDef.substitute(substitutionsMap)
			# wrapperDefWrite(tmp)
			# wrappersDefFile.write(tmp)

			if not instanceName in classCache:
				classCache[instanceName] = ''

			classCache[instanceName] += tmp

	for k, v in classCache.items():
		storeClass(k, v)

class RsClass:
	_params = []
	_kind = None

	def __init__(self, name):
		self._params = []
		self.name = name.replace("::", "_")

	# Generate docstring for the RsClass
	def generateDocString(self):
		txt = "".join('\n' + tab + tab + ':param ' + p._name + ': ' + '(' + p.type + ')' for p in self._params)
		txt += "".join('\n' + tab + tab + ':type ' + p._name + ': ' + p.type for p in self._params)
		return txt

	def getOrderedParams(self):
		return ", ".join(str(p._name) + " = None" for p in self._params)

	def getParamAssign(self):
		return "".join('\n' + tab + tab + 'self.' + p._name + ' = ' + p._name for p in self._params)

	# Only work if _kind is enum
	def generateEnumValues(self):
		if self._kind is not "enum":
			return None
		return "".join('\n' + tab + k + v for k,v in self._params[0].items())


# Process a rfid if is not on rsClassMap or is not processed before
# If it finish with a _ + 34 len string it call  parseMemberClass else parseCompoundClass
def processNextRfid(rfid):
	# Check if this class is already parsed
	if rfid in rsClassMap and isinstance(rsClassMap[rfid], RsClass):
		return

	# Check if the rfidReference contain the hex hash that indicates a memberdef reference inside the xml file
	if isMemberClassRfid(rfid):
		parseMemberClass(getRfidFile(rfid), rfid)
	else:
		parseCompoundClass(getRfidFile(rfid), rfid)

# Get the "file name" from an rfid parsing the rfid
def getRfidFile(rfid):
	# Check if the rfidReference contain the hex hash that indicates a memberdef reference inside the xml file
	if isMemberClassRfid(rfid):
		return rfid.rsplit('_', 1)[0]
	return rfid

# Return True if it finish with a 34 digit len hash
def isMemberClassRfid(rfid):
	tmp = rfid.rsplit('_', 1)
	if len(tmp) > 1 and tmp[1].__len__() == 34:
		return True
	return False

def getDomFromRfidFile(rfidFile):
	try:
		return ET.parse(doxPrefix + rfidFile + '.xml').getroot()
	except FileNotFoundError:
		print('Can\'t open:', doxPrefix + rfidFile + '.xml on aux RsClass generation')
		sys.exit()

# Get memberdef xml object from an rfid.
def getMemberdefFromRfid(rfid):
	dom = getDomFromRfidFile(getRfidFile(rfid))
	for md in dom.findall('.//memberdef'):
		if md.attrib['id'] == rfid:
			return md

# Parse a compound class and return an instance of RsClass with the data
def parseCompoundClass(rfidFile, rfidReference):
	print("Parsing compound class", rfidFile, rfidReference)

	dom = getDomFromRfidFile(rfidFile)

	name = dom.find('.//compoundname').text

	rsClass = RsClass(name)
	for md in dom.findall('.//memberdef'):
		# Get all parameters that are variables
		if md.attrib['kind'] != 'variable':
			continue

		tmpParam = parseMemberdefToMethodParam(md)

		# If rfid is not None just run recursively the parameter creation
		if tmpParam._rfid is not None:
			processNextRfid(tmpParam._rfid)

		rsClass._params.append(tmpParam)

	rsClassMap[rfidReference] = rsClass

# Parse a member class, considered all that rfid finish with a _ + 34 len string
def parseMemberClass(rfidFile, rfidReference):
	print("Parsing member class", rfidFile, rfidReference)
	name = None
	md = getMemberdefFromRfid(rfidReference)
	name =  md.find('.//name').text
	rsClass = RsClass(name)

	if md.attrib['kind'] == "typedef":
		tmpParam = parseMemberdefToMethodParam(md)
		tmpParam._type = typeDefGetPrimitiveType(md.find('type'),)
		rsClass._params.append(tmpParam)
		rsClass._kind = "typedef"

	elif md.attrib['kind'] == "enum":
		rsClass._kind = "enum"
		enumValues = {}
		for val in md.findall('.//enumvalue'):
			# Todo: some enums hasn't initializer, see rsevents_8h_1aebdc15781f2ab61f46840db2bff25ee9
			if val.find('initializer') is not None and val.find('name') is not None:
				enumValues[val.find('name').text] = val.find('initializer').text
		rsClass._params.append(enumValues)

	elif md.attrib['kind'] == "define":
		# Define are not needed
		return
	else:
		print("Error: not supported class kind: ", md.attrib['kind'])
		sys.exit(1)
	rsClassMap[rfidReference] = rsClass

# Function that recursively dig onto a memberdef type until found it primitive type
def typeDefGetPrimitiveType(typeTag, parentRfid = None):
	rfid = typeTag.find('ref').attrib['refid'] if typeTag.find('ref') != None else None

	# If is a pair get the inner rfid references to store it
	if typeTag.text is not None and typeTag.text.startswith("std::pair"):
		pairTypes = []
		for ref in typeTag.findall('ref'):
			processNextRfid(ref.attrib['refid'])
			pairTypes.append(ref.text)
		return pairTypes

	# If rfid is not None just run recursively the parameter creation
	elif rfid is not None:
		if rfid in PRIMITIVECLASS_MAP:
			return PRIMITIVECLASS_MAP[rfid]
		# Sometimes happen that the "memberdef" rfid is the same as the type "rfid"
		# See for example p3gossipdiscovery_8h_1a6be3a190b47f7623fac9e537acee1b55 where the definition is:
		# using RsPeerId =  RsPeerId
		# https://github.com/RetroShare/RetroShare/issues/1602
		elif parentRfid is not None and parentRfid == rfid:
			print("Error parsing typeDefGetPrimitiveType: memberdef.rfid == type.rfid on")
			print(parentRfid)
			sys.exit(1)
		else:
			md = getMemberdefFromRfid(rfid)
			return typeDefGetPrimitiveType(md.find('type'), rfid)
	else:
		return getText(typeTag)

# Parse information in a memberdef object to get viarious data
def parseMemberdefToMethodParam(md):
	tmpParam = MethodParam()
	tmpParam._name = md.find('name').text
	tmpParam._rfid = md.find('type/ref').attrib['refid'] if md.find('type/ref') != None else None
	tmpDefval = md.find('defval')
	tmpParam._defval = getText(tmpDefval) if tmpDefval != None else ''
	tmpParam._type = getText(md.find('type'))
	tmpParam._description = getText(md.find('briefdescription'))
	return tmpParam

def initializeClassesFile():
	wrapperDefWrite("import json")
	wrapperDefWrite("\nfrom enum import Enum")
	wrapperDefWrite("\nfrom typing import NewType, Tuple")

# Parse de map rsClassFileMap with the detected compound classes
def parseAuxClasses():
	tmpClassMap = rsClassMap.copy()
	for rfid in tmpClassMap:
		processNextRfid(rfid)

	# Need to do it in different loops due to the inner compound classes that are add after parsing
	for cls in rsClassMap.values():
		substitutionsMap = dict()
		substitutionsMap["className"] = cls.name
		templFilePath = None

		if cls._kind == "enum":
			substitutionsMap["enumValues"] = cls.generateEnumValues()
			templFilePath = TEMPLATE_RSCLASS_ENUM
		elif cls._kind == "typedef":
			param = cls._params[0]
			if isinstance(param._type, list):
				wrapperDefWrite("\n\n" + cls.name + " = Tuple[" + ",".join(p for p in param._type) + "]")
			elif isinstance(param._type, str):
				wrapperDefWrite("\n\n" + cls.name + " = NewType('" + cls.name + "', " + param.type + ")")
			else:
				print("Error: not supported type of parameter", type(param._type))
				sys.exit(1)
			continue
		else:
			substitutionsMap["paramsDocstring"] = cls.generateDocString()
			substitutionsMap["className"] = cls.name
			substitutionsMap["orderedParams"] = cls.getOrderedParams()
			substitutionsMap["params"] = cls.getParamAssign()
			templFilePath = TEMPLATE_RSCLASS

		print("Storing RsClass:" , cls.name)
		templFile = open(templFilePath, 'r')
		wrapperDef = TemplateOwn(templFile.read())
		wrapperDefWrite(wrapperDef.substitute(substitutionsMap))



if len(sys.argv) != 2:
	print('Usage:', sys.argv[0], 'XML_PATH')
	sys.exit()

start_time = time.time()

xmlPath = str(sys.argv[1])
doxPrefix = xmlPath + '/xml/'

OUTPUTLIB='wrapper_python.py' # Output of the api wrapper library
# OUTPUTLIB = xmlPath + '/jsonapipython-wrappers.inl'
OUTPUTCLASSWRAPPER='wrapper_class_python.py' # Output of the RsClass wrapper

TEMPLATE='tmpl/python/python.tmpl' # Tamplate for sync functions
TEMPLATE_ASYNC='tmpl/python/python-async.tmpl' # Template for async functions
TEMPLATE_RSCLASS='tmpl/python/python-rsclass.tmpl' # Template for RsClass wrapper
TEMPLATE_RSCLASS_ENUM='tmpl/python/python-rsclass-enum.tmpl' # Template for RsClass enum type
HEADERTMPL='tmpl/python/python-header.tmpl'  # Header template for api wrapper library

# Used to create RsClasses, this map the RS metaclasses to python primitive types
# Where the key is the rfid of the data type in question
PRIMITIVECLASS_MAP = {
	'structt__RsGenericIdType': 'str',
	'classt__RsFlags32': 'int',
	# This is needed due a bug on doxygen parse:
	# https://github.com/RetroShare/RetroShare/issues/1602
	'p3gossipdiscovery_8h_1a6be3a190b47f7623fac9e537acee1b55': 'str', # RsPeerId
	'p3gossipdiscovery_8h_1ac237a6e20b1ea729aa9d585dd3514f07': 'str', # RsPgpId
}

tab = "    " # Used to solve indentation spaces when template is filled

try:
	wrappersDefFile = open(OUTPUTLIB, 'w')
except FileNotFoundError:
	print('Can\'t open:', OUTPUTLIB)

wrapperDefWrite(open(HEADERTMPL, 'r').read())

# Used to store the parameters that maybe are classes with mMeta, for example rsGxsForumMsg
rsClassMap = {}

try:
	for file in os.listdir(doxPrefix):
		if file.endswith("8h.xml"):
			processFile(os.path.join(doxPrefix, file))
except FileNotFoundError:
	print('Can\'t list:', doxPrefix)

try:
	wrappersDefFile = open(OUTPUTCLASSWRAPPER, 'w')
except FileNotFoundError:
	print('Can\'t open:', OUTPUTCLASSWRAPPER)

initializeClassesFile() # Add header to the RsClass wrapper like imports and others
parseAuxClasses()

print("--- %s seconds ---" % (time.time() - start_time))
